package com.mayichat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MayiChatApiYiyanApplication {
    public static void main(String[] args) {
        SpringApplication.run(MayiChatApiYiyanApplication.class, args);
        System.out.println("蚂蚁一言API启动成功");
    }
}
