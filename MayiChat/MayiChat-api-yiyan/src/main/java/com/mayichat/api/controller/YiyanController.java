package com.mayichat.api.controller;

import com.mayichat.common.annotation.Anonymous;
import com.mayichat.api.service.YiyanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/yiyan/v1")
@Slf4j
public class YiyanController {



    @Autowired
    YiyanService yiyan;
    @Anonymous
    @GetMapping("/getyiyan")
    public String getyiyan(){
        System.out.println("aaaaa");
        return yiyan.Getyiyan();
    }


}
