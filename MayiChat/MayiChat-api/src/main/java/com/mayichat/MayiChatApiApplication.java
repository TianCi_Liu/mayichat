package com.mayichat;

import io.github.asleepyfish.annotation.EnableChatGPT;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@EnableChatGPT
public class MayiChatApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MayiChatApiApplication.class, args);
        System.out.println("蚂蚁ChatAPI启动成功");
    }

}
