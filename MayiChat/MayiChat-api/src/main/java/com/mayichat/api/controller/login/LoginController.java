package com.mayichat.api.controller.login;

import cn.hutool.Hutool;
import cn.hutool.core.lang.Validator;
import com.alibaba.fastjson2.JSONObject;
import com.mayichat.api.entity.user.User;
import com.mayichat.api.service.loginService;
import com.mayichat.api.utils.EmailServer;
import com.mayichat.common.core.domain.AjaxResult;
import com.mayichat.common.core.redis.RedisCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author LiuTianci
 * @description TODO
 * @date 2023-06-21 19:29
 */

@RestController
@RequestMapping("/v1")
@Slf4j
public class LoginController {

    @Autowired
    EmailServer emailServer;

    @Autowired
    RedisCache redisCache;

    @Autowired
    loginService loginService;

    @GetMapping("gethost")
    public AjaxResult gethost(HttpServletRequest request){
        String ip = request.getHeader("X-Real-IP");
        if (ip != null && !"".equals(ip) && !"unknown".equalsIgnoreCase(ip)) {
            return AjaxResult.success(ip);
        }
        ip = request.getHeader("X-Forwarded-For");
        if (ip != null && !"".equals(ip) && !"unknown".equalsIgnoreCase(ip)) {
            int index = ip.indexOf(',');
            if (index != -1) {
                return AjaxResult.success(ip.substring(0, index));
            } else {
                return AjaxResult.success(ip);
            }
        } else {
            return AjaxResult.success(request.getRemoteAddr());
        }
    }

    @PostMapping("/getcode")
    public AjaxResult getcode(@RequestBody String user){
        JSONObject jsonObject = JSONObject.parse(user);
        String email = jsonObject.get("user").toString();
        boolean isemail = Validator.isEmail(email);
        if (!isemail){
            return AjaxResult.error("验证码格式错误");
        }

        //验证邮箱格式是否正确 使用工具
        /**
         * 判断是否已有 如果已有则删除
         */
        if (redisCache.getCacheObject(email) != null){
            redisCache.deleteObject(email);
        }
        String title = "蚂蚁ChatAI服务助手";
        String code = generateVerificationCode();
        String emaillogin = emailServer.Emaillogin(email, title, code);
        if ("60".equals(emaillogin)) {
            log.info("注册验证"+emaillogin);
            //redis 保存
            try {
                redisCache.setCacheObject(email,code,10,TimeUnit.MINUTES);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            return AjaxResult.success("发送成功");
        }
        return AjaxResult.error("验证码发送失败");
    }


    public static String generateVerificationCode() {
        Random random = new Random();
        int code = random.nextInt(900000) + 100000;
        return String.valueOf(code);
    }

    //注册 登录
    @PostMapping("/mayiregister")
    //使用实体类User接惨
    public AjaxResult mayiregister(@RequestBody User user){
        AjaxResult login = loginService.login(user);
        return login;
    }
}
