package com.mayichat.api.controller.gpt;

import com.alibaba.fastjson2.JSONObject;
import com.mayichat.api.service.ChatService;
import com.mayichat.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;

import java.util.List;


/**
 * @author LiuTC
 * @title chat
 * @create 2023/8/19 17:10
 **/

@RestController
@RequestMapping("/v1")
public class chat {

    @Autowired
    ChatService chatService;

    @PostMapping("/xfChat")
    public ResponseBodyEmitter xfChat(@RequestBody String msg){
        JSONObject j = JSONObject.parse(msg);
        String label = j.get("label").toString();
        String uid = j.get("label").toString();
        String chatid = j.get("chatid").toString();
        String createTime = j.get("createTime").toString();
        String enable = j.get("enable").toString();
        ResponseBodyEmitter xfChatSseReq = chatService.xfchat(label,uid);
        return xfChatSseReq;
    }

    @GetMapping("getChatInfo")
    public AjaxResult getChatInfo(String token){
        return AjaxResult.success("okTest");
    }

}
