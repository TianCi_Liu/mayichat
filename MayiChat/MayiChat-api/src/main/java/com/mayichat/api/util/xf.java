package com.mayichat.api.util;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.google.gson.JsonArray;
import com.mayichat.api.entity.xf.Payload;
import com.mayichat.api.entity.xf.ResponseData;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author LiuTianci
 * @description TODO
 * @date 2023-06-21 18:04
 */

//科大讯飞星火大模型 封装
@Component
@Slf4j
public class xf extends WebSocketListener{

//    @Value("${xf.hostUrl}")
//    private String hostUrl = "";
//
//    @Value("${xf.APPID}")
//    private String APPID;
//
//    @Value("${xf.APIKEY}")
//    private String APIKEY;
//
//    @Value("${xf.APISecret}")
//    private String APISecret;
//    @Value("${xf.hostUrl}")
private String hostUrl = "https://spark-api.xf-yun.com/v1.1/chat";

    //    @Value("${xf.APPID}")
    private String APPID ="d2bef235";

    //    @Value("${xf.APIKEY}")
    private String APIKEY ="94e4adbaff43f20d47c43912b74243e2";

    //    @Value("${xf.APISecret}")
    private String APISecret="ZWNiZTJlZDNmYTk5YTliMDFiNDQ1N2Q4";

//    public static String hostUrl = "https://spark-api.xf-yun.com/v1.1/chat";
//    public static String APPID = "d2bef235";//从开放平台控制台中获取
//    public static String APIKEY = "94e4adbaff43f20d47c43912b74243e2";//从开放平台控制台中获取
//    public static String APISecret = "ZWNiZTJlZDNmYTk5YTliMDFiNDQ1N2Q4";//从开放平台控制台中获取

    public static final Gson json = new Gson();
    public static String answer = "";

    public String temps = "";
    private static String question;

    public String xinghuo(String question){
        if (question != null){
            xf.question = question;
            log.info("问题："+xf.question);
            try {
                //构建鉴权httpurl
                String authUrl = getAuthorizationUrl(hostUrl,APIKEY,APISecret);
                OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
                String url = authUrl.replace("https://","wss://").replace("http://","ws://");
                Request request = new Request.Builder().url(url).build();
                WebSocket webSocket = okHttpClient.newWebSocket(request,new xf());
                while (true) {
                    if (!answer.isEmpty()) {
                        return answer;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                answer = "";
            }
        }
      return null;
    };


    //鉴权url
    public static String  getAuthorizationUrl(String hostUrl , String apikey ,String apisecret) throws Exception {
        //获取host
        URL url = new URL(hostUrl);
        //获取鉴权时间 date
        SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);

        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        String date = format.format(new Date());
        //获取signature_origin字段
        StringBuilder builder = new StringBuilder("host: ").append(url.getHost()).append("\n").
                append("date: ").append(date).append("\n").
                append("GET ").append(url.getPath()).append(" HTTP/1.1");

        //获得signatue
        Charset charset = Charset.forName("UTF-8");
        Mac mac = Mac.getInstance("hmacsha256");
        SecretKeySpec sp = new SecretKeySpec(apisecret.getBytes(charset),"hmacsha256");
        mac.init(sp);
        byte[] basebefore = mac.doFinal(builder.toString().getBytes(charset));
        String signature = Base64.getEncoder().encodeToString(basebefore);
        //获得 authorization_origin
        String authorization_origin = String.format("api_key=\"%s\",algorithm=\"%s\",headers=\"%s\",signature=\"%s\"",apikey,"hmac-sha256","host date request-line",signature);
        //获得authorization
        String authorization = Base64.getEncoder().encodeToString(authorization_origin.getBytes(charset));
        //获取httpurl
        HttpUrl httpUrl = HttpUrl.parse("https://" + url.getHost() + url.getPath()).newBuilder().//
                addQueryParameter("authorization", authorization).//
                addQueryParameter("date", date).//
                addQueryParameter("host", url.getHost()).//
                build();

        return httpUrl.toString();
    }


    public void open (){
        WebSocketListener webSocketListener = new WebSocketListener() {
            @Override
            public void onOpen(WebSocket webSocket, Response response) {
                super.onOpen(webSocket, response);
                new Thread(()->{
                    JsonObject frame = new JsonObject();
                    JsonObject header = new JsonObject();
                    JsonObject chat = new JsonObject();
                    JsonObject parameter = new JsonObject();
                    JsonObject payload = new JsonObject();
                    JsonObject message = new JsonObject();
                    JsonObject text = new JsonObject();
                    JsonArray ja = new JsonArray();

                    //填充header
                    header.addProperty("app_id",APPID);
                    header.addProperty("uid","123456789");
                    //填充parameter
                    chat.addProperty("domain","general");
                    chat.addProperty("random_threshold",0);
                    chat.addProperty("max_tokens",1024);
                    chat.addProperty("auditing","default");
                    parameter.add("chat",chat);
                    //填充payload
                    text.addProperty("role","user");
                    text.addProperty("content",question);
                    ja.add(text);
//            message.addProperty("text",ja.getAsString());
                    message.add("text",ja);
                    payload.add("message",message);
                    frame.add("header",header);
                    frame.add("parameter",parameter);
                    frame.add("payload",payload);
                    webSocket.send(frame.toString());
                }).start();
            };
        };
    }
    //重写onopen
    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        super.onOpen(webSocket, response);
        new Thread(()->{
            JsonObject frame = new JsonObject();
            JsonObject header = new JsonObject();
            JsonObject chat = new JsonObject();
            JsonObject parameter = new JsonObject();
            JsonObject payload = new JsonObject();
            JsonObject message = new JsonObject();
            JsonObject text = new JsonObject();
            JsonArray ja = new JsonArray();

            //填充header
            header.addProperty("app_id",APPID);
            header.addProperty("uid","123456789");
            //填充parameter
            chat.addProperty("domain","general");
            chat.addProperty("random_threshold",0);
            chat.addProperty("max_tokens",1024);
            chat.addProperty("auditing","default");
            parameter.add("chat",chat);
            //填充payload
            text.addProperty("role","user");
            text.addProperty("content",question);
            ja.add(text);
//            message.addProperty("text",ja.getAsString());
            message.add("text",ja);
            payload.add("message",message);
            frame.add("header",header);
            frame.add("parameter",parameter);
            frame.add("payload",payload);

            webSocket.send(frame.toString());
        }


        ).start();
    }

    //重写onmessage
    @Override
    public void onMessage(WebSocket webSocket, String text) {

        super.onMessage(webSocket, text);

        ResponseData responseData = json.fromJson(text,ResponseData.class);
        if(0 == responseData.getHeader().get("code").getAsInt()){
            if(2 != responseData.getHeader().get("status").getAsInt()){
                Payload pl = json.fromJson(responseData.getPayload(),Payload.class);
                JsonArray temp = (JsonArray) pl.getChoices().get("text");
                JsonObject jo = (JsonObject) temp.get(0);
                temps += jo.get("content").getAsString();
            }else {
                Payload pl1 = json.fromJson(responseData.getPayload(),Payload.class);
                JsonObject jsonObject = (JsonObject) pl1.getUsage().get("text");
                int prompt_tokens = jsonObject.get("prompt_tokens").getAsInt();
                JsonArray temp1 = (JsonArray) pl1.getChoices().get("text");
                JsonObject jo = (JsonObject) temp1.get(0);
                temps += jo.get("content").getAsString();
                log.info("返回结果为：\n" + temps);
                answer = "";
                answer = temps;
                webSocket.close(3,"客户端主动断开链接");
            }
        }else{
            log.info("返回结果错误：\n" + responseData.getHeader().get("code") +  responseData.getHeader().get("message") );
        }
    }

    //重写onFailure

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        super.onFailure(webSocket, t, response);
        log.info(String.valueOf(response));
    }
}
