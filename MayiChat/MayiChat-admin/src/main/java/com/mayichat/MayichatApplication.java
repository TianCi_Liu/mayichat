package com.mayichat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author mayichat
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class MayichatApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(MayichatApplication.class, args);
        System.out.println("蚂蚁Chat后端管理平台系统启动成功");
    }
}
