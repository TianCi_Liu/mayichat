package com.mayichat.api.entity.xf;

import com.google.gson.JsonObject;

/**
 * @author LiuTianci
 * @description TODO
 * @date 2023-06-21 18:14
 */
public class Payload {

    private JsonObject choices;
    private JsonObject usage;

    public JsonObject getChoices() {
        return choices;
    }

    public JsonObject getUsage() {
        return usage;
    }


}
