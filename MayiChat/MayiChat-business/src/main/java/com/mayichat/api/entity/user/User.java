package com.mayichat.api.entity.user;


import lombok.Data;

@Data
public class User {

    //id
    private Long id;
    //uid
    private String muid;
    // 邮箱
    private String userEmail;
    //密码
    private String password;
    //所属对话列表id
    private String chatGroupId;
    //验证码
    private String code;

    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 登录时间
     */
    private Long loginTime;

    /**
     * 过期时间
     */
    private Long expireTime;

    /**
     * 登录IP地址
     */
    private String ipaddr;
}
