package com.mayichat.api.entity.user;

/**
 * @author LiuTianci
 * @description TODO
 * @date 2023-07-18 15:02
 */
public class ChUser {

    private String msg;

    private String uid;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
