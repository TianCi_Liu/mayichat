package com.mayichat.api.entity.xf;

/**
 * @author LiuTianci
 * @description TODO
 * @date 2023-06-21 18:14
 */
public class Header {

    private int code ;
    private String message;
    private String sid;
    private String status;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getSid() {
        return sid;
    }

    public String getStatus() {
        return status;
    }

}
