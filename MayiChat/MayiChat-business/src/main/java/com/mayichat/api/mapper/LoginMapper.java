package com.mayichat.api.mapper;

import com.mayichat.api.entity.user.User;

/**
 * @author LiuTC
 * @title LoginMapper
 * @create 2023/8/24 15:43
 **/
public interface LoginMapper {

    String unregister(String email);

    //注册
    int register(User user);

    //登录
    User login(User user);
    //获取用户信息

    User getUserInfo(String token);

}
