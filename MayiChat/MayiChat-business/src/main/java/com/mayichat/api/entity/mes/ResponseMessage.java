package com.mayichat.api.entity.mes;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author LiuTianci
 * @description TODO
 * @date 2023-07-22 16:14
 * 后端相应 实体类
 */
@Data
public class ResponseMessage {

    /**
     * 自增id
     */
    private long id;

    //接收方id
    private String uid;

    //接收方名字
    private String name;

    //相应内容
    private String message;


    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 备注 */
    private String remark;



}
