package com.mayichat.api.utils;


import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;

import java.util.*;

/**
 *@title xf_spark
 *@author LiuTC
 *@create 2023/8/18 19:31
 *
 *
 *星火大模型调用接口 最终版
 *
 *
 **/


public class xf_spark {

    public static void main(String[] args) {

        String requestJson;

        requestJson = "{\n" +
                "  \"header\": {\n" +
                "    \"app_id\": \"" + "appID" + "\",\n" +
                "    \"uid\": \"" + UUID.randomUUID().toString().substring(0, 10) + "\"\n" +
                "  },\n" +
                "  \"parameter\": {\n" +
                "    \"chat\": {\n" +
                "      \"domain\": \"generalv2\",\n" +
                "      \"temperature\": 1,\n" +
                "      \"max_tokens\": 1024\n" +
                "    }\n" +
                "  },\n" +
                "  \"payload\": {\n" +
                "    \"message\": {\n" +
                "      \"text\": [\n" +
                "        {\n" +
                "          \"role\": \"user\",\n" +
                "          \"content\": \"中国第一个皇帝是谁？\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"role\": \"assistant\",\n" +
                "          \"content\": \"秦始皇\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"role\": \"user\",\n" +
                "          \"content\": \"秦始皇修的长城吗\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"role\": \"assistant\",\n" +
                "          \"content\": \"是的\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"role\": \"user\",\n" +
                "          \"content\": \"" + "new msg" + "\"\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "  }\n" +
                "}";

//        System.out.println(requestJson);

        JSONObject jsonList = new JSONObject();

        JSONObject headerJson = new JSONObject();
        headerJson.put("app_id","appid");
        headerJson.put("uid",UUID.randomUUID().toString().substring(0, 10));


        JSONObject parameterJson = new JSONObject();
        JSONObject parameterDateJson = new JSONObject();
        parameterDateJson.put("domain","generalv2");
        parameterDateJson.put("temperature",1);
        parameterDateJson.put("max_tokens",1024);
        parameterJson.put("chat",parameterDateJson);
        jsonList.put("header",headerJson);
        jsonList.put("parameter",parameterJson);
        JSONObject payloadJson = new JSONObject();
        JSONObject payloadMessageJson = new JSONObject();
        JSONArray payloadMessageTextJson = new JSONArray();
        List<Map<String,String>> list = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("role","user");
        map.put("content","中国第一个皇帝是谁");
        Map<String, String> map1 = new HashMap<>();
        map1.put("role","assistant");
        map1.put("content","秦始皇");
        list.add(map);
        list.add(map1);
        for (Map<String, String> stringStringMap : list) {
            payloadMessageTextJson.add(stringStringMap);
        }
        payloadMessageJson.put("text",payloadMessageTextJson);
        payloadJson.put("message",payloadMessageJson);
        jsonList.put("payload",payloadJson);
        System.out.println(jsonList);

    }


}
