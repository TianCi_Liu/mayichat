package com.mayichat.api.entity.user;

/**
 * @author LiuTC
 * @title ChatMessage
 * @create 2023/8/19 16:15
 **/
public class ChatMessage {

    private String senderId;
    private String content;

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
