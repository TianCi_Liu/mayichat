package com.mayichat.api.utils;

import cn.hutool.extra.mail.MailUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmailServer {

    public String Emaillogin(String email,String title,String msg){
        try {
            String html = "<html>\n" +
                    "<p>尊敬的：<span style=\"font-weight: bold\">"+email+"</span> 你好！</p>\n" +
                    "<p>您正在申请 蚂蚁ChatAI服务助手体验，您的邮箱验证码:</p>\n" +
                    "<h2>"+msg+"</h2>\n" +
                    "<p>验证码将于10分钟后过期，请及时输入验证。如果不是您本人注册申请，请忽略本邮件。</p>\n" +
                    "<p>————————————————————————————————————</p>\n" +
                    "<p>本邮件为系统自动发送，请勿回复。</p>\n" +
                    "</html>";
            String send = MailUtil.sendHtml(email, title, html);
            return "60";
        }catch (Exception e){
            return "61";
        }
    }

//    public static void main(String[] args) {
//        String s = Emaillogin("2313835217@qq.com", "蚂蚁ChatAI服务助手", "12345");
//        System.out.println(s);
//    }

}
