package com.mayichat.api.service;

import com.mayichat.api.entity.user.ChUser;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;

/**
 * @author LiuTianci
 * @description TODO
 * @date 2023-07-18 15:10
 */
public interface ChatService {

    ResponseBodyEmitter xfchat(String msg,String uid);
}
