package com.mayichat.api.service.impl;

import com.mayichat.api.service.ChatService;

import com.unfbx.sparkdesk.SparkDeskClient;
import com.unfbx.sparkdesk.constant.SparkDesk;
import com.unfbx.sparkdesk.entity.*;
import com.unfbx.sparkdesk.listener.ChatListener;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;

/**
 * @author LiuTianci
 * @description TODO
 * @date 2023-07-18 15:10
 */

@Service
@Slf4j
public class ChatServiceimpl implements ChatService {

    @Value("${xf.hostUrl}")
    String hostUrl;

    @Value("${xf.APPID}")
    String APPID;

    @Value("${xf.APISecret}")
    String APISecret;

    @Value("${xf.APIKey}")
    String APIKey;
     static String remsgg = "成功";
    StringBuilder sb = new StringBuilder();
    @Override
    public ResponseBodyEmitter xfchat(String msg,String uid) {
        ResponseBodyEmitter emitter = new ResponseBodyEmitter();
        //在后台线程中生成数据并发送
        CompletableFuture.runAsync(() ->{
            SparkDeskClient xf = SparkDeskClient.builder()
                    .host(SparkDesk.SPARK_API_HOST_WS_V1_1)
                    .appid(APPID)
                    .apiKey(APIKey)
                    .apiSecret(APISecret)
                    .build();
            //构建请求参数
            InHeader header = InHeader.builder().uid(UUID.randomUUID().toString().substring(0, 10)).appid("d2bef235").build();
            Parameter parameter = Parameter.builder()
                    .chat(Chat.builder()
                            .domain("general")
                            .maxTokens(2048)
                            .temperature(0.3)
                            .chatId(uid).build())
                    .build();
            List<Text> text = new ArrayList<>();
            text.add(Text.builder().role(Text.Role.USER.getName()).content(msg).build());
            InPayload payload = InPayload.builder().message(Message.builder().text(text).build()).build();
            AIChatRequest aiChatRequest = AIChatRequest.builder().header(header).parameter(parameter).payload(payload).build();

            xf.chat(new ChatListener(aiChatRequest) {
                //异常回调
                @SneakyThrows
                @Override
                public void onChatError(AIChatResponse aiChatResponse) {
                    log.warn(String.valueOf(aiChatResponse));
                }
                //输出回调
                @Override
                public void onChatOutput(AIChatResponse aiChatResponse) {
                    sb.append(aiChatResponse.getPayload().getChoices().getText().get(0).getContent());
                    log.info("content: " + aiChatResponse);
                }
                //会话结束回调
                @Override
                public void onChatEnd() {
                    log.info("当前会话结束了");
                    log.info(sb.toString());
                    try {
                        emitter.send(sb.toString());
                        sb.setLength(0);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }

                    emitter.complete();
                }
                //会话结束 获取token使用信息回调
                @Override
                public void onChatToken(Usage usage) {
                    log.info("token 信息：" + usage);
                }
            });

        });
        return emitter;
    }



}
