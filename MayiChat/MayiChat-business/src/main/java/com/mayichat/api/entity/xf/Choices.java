package com.mayichat.api.entity.xf;

import com.google.gson.JsonArray;

/**
 * @author LiuTianci
 * @description TODO
 * @date 2023-06-21 18:15
 */
public class Choices {
    private int status;
    private int seq;
    private JsonArray text;

    public int getStatus() {
        return status;
    }

    public int getSeq() {
        return seq;
    }

    public JsonArray getText() {
        return text;
    }

}
