package com.mayichat.api.entity.mes;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mayichat.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @author LiuTianci
 * @description TODO
 * @date 2023-07-22 16:11
 *
 * 请求端 实体类
 */
@Data
public class RequestMessage extends BaseEntity {

    /**
     * 自增id
     */
    private long id;

    //用户唯一id
    private String uid;

    //用户名称 发送方
    private String name;

    //用户消息
    private String message;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 备注 */
    private String remark;

}
