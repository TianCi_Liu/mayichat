package com.mayichat.api.service.impl;

import com.mayichat.api.entity.user.User;
import com.mayichat.api.mapper.LoginMapper;
import com.mayichat.api.service.loginService;
import com.mayichat.api.utils.JwtUtils;
import com.mayichat.common.core.domain.AjaxResult;
import com.mayichat.common.core.redis.RedisCache;
import com.mayichat.common.utils.uuid.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author LiuTC
 * @title loginServiceImpl
 * @create 2023/8/24 16:42
 **/
@Service
@Slf4j
public class loginServiceImpl implements loginService {


    @Autowired
    private LoginMapper loginMapper;
    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    RedisCache redisCache;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult login(User user) {
        //验证验证码
        Object cacheObject = redisCache.getCacheObject(user.getUserEmail());
        if (cacheObject == null){
            throw new RuntimeException("验证码无效或已过期");
        }
        String recode = cacheObject.toString();
        if (!recode.equals(user.getCode())) {
            throw new RuntimeException("验证码错误");
        }
        //判断用户是否注册
        String unregister = loginMapper.unregister(user.getUserEmail());
        if (user.getUserEmail().equals(unregister)) {
            log.info("已注册" + user.getUserEmail());
            User userInfo = loginMapper.getUserInfo(user.getUserEmail());
            //生成token
            String token = jwtUtils.createToken(user);
            //获取用户信息
            AjaxResult req = new AjaxResult();
            req.put("code",200);
            req.put("token", token);
            req.put("msg", "登录成功");
            req.put("userData", userInfo);
            return req;
        } else {
            //注册
            User u = new User();
            u.setUserEmail(user.getUserEmail());
            u.setCode(user.getCode());
            String uid = UUID.randomUUID().toString();
            u.setMuid(uid);
            //塞入数据库
            int register = loginMapper.register(u);
            User userInfo = loginMapper.getUserInfo(u.getUserEmail());
            //设置token
            String token = jwtUtils.createToken(u);
            //返回登录内容
            AjaxResult req = new AjaxResult();
            req.put("token", token);
            req.put("msg", "登录成功");
            req.put("userData", userInfo);
            return req;
        }
    }
}
