package com.mayichat.api.service;

import com.mayichat.api.entity.user.User;
import com.mayichat.common.core.domain.AjaxResult;

/**
 * @author LiuTC
 * @title loginService
 * @create 2023/8/24 16:41
 **/
public interface loginService {


    //登录
    AjaxResult login(User user);

}
