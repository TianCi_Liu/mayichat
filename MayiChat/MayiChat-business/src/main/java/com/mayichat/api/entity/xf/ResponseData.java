package com.mayichat.api.entity.xf;

import com.google.gson.JsonObject;

/**
 * @author LiuTianci
 * @description TODO
 * @date 2023-06-21 18:14
 */
public class ResponseData {

    private JsonObject header;
    private  JsonObject payload;

    public JsonObject getHeader() {
        return header;
    }

    public JsonObject getPayload() {
        return payload;
    }


}
