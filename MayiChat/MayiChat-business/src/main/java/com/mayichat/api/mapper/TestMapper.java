package com.mayichat.api.mapper;

/**
 * @author LiuTianci
 * @description TODO
 * @date 2023-07-22 16:34
 */
public interface TestMapper {

   public Integer send(String msg);
}
