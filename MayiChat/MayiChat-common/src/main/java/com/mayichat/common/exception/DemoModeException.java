package com.mayichat.common.exception;

/**
 * 演示模式异常
 * 
 * @author mayichat
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
