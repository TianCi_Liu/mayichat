package com.mayichat.common.enums;

/**
 * 数据源
 * 
 * @author mayichat
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
