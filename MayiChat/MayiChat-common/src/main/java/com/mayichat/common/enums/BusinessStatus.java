package com.mayichat.common.enums;

/**
 * 操作状态
 * 
 * @author mayichat
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
